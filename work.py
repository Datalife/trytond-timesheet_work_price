# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal
from trytond.cache import Cache
from trytond.model import fields, ModelView, ModelSQL, Unique
from trytond.pool import PoolMeta, Pool
from trytond.modules.timesheet_cost.timesheet import price_digits
from trytond.transaction import Transaction


class Work(metaclass=PoolMeta):
    __name__ = 'timesheet.work'

    _cost_prices_cache = Cache('timesheet_work.cost_prices')
    cost_price = fields.Function(fields.Numeric('Cost Price',
            digits=price_digits), 'get_cost_price')
    cost_prices = fields.One2Many('timesheet.work_cost_price', 'work',
        'Cost Prices')

    def get_cost_price(self, name):
        '''
        Return the cost price at the date given in the context or the
        current date
        '''
        ctx_date = Transaction().context.get('date', None)
        return self.compute_cost_price(ctx_date)

    def get_work_costs(self):
        '''Return a sorted list by date of start date and cost_price'''
        pool = Pool()
        CostPrice = pool.get('timesheet.work_cost_price')
        # Get from cache employee costs or fetch them from the db
        work_costs = self._cost_prices_cache.get(self.id)
        if work_costs is None:
            cost_prices = CostPrice.search([
                    ('work', '=', self.id),
                    ], order=[('date', 'ASC')])

            work_costs = []
            for cost_price in cost_prices:
                work_costs.append(
                    (cost_price.date, cost_price.cost_price))
            self._cost_prices_cache.set(self.id, work_costs)
        return work_costs

    def compute_cost_price(self, date=None):
        "Return the cost price at the given date"
        pool = Pool()
        Date = pool.get('ir.date')

        work_costs = self.get_work_costs()

        if date is None:
            date = Date.today()
        # compute the cost price for the given date
        cost = 0
        if work_costs and date >= work_costs[0][0]:
            for edate, ecost in work_costs:
                if date >= edate:
                    cost = ecost
                else:
                    break
        return cost


class WorkCostPrice(ModelSQL, ModelView):
    '''Work Cost Price'''
    __name__ = 'timesheet.work_cost_price'

    date = fields.Date('Date', required=True, select=True)
    cost_price = fields.Numeric('Cost Price', digits=price_digits,
        required=True)
    work = fields.Many2One('timesheet.work', 'Work', required=True,
        select=True, ondelete='CASCADE')

    @classmethod
    def __setup__(cls):
        super(WorkCostPrice, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('work_date_uniq',
                Unique(t, t.work, t.date),
                'timesheet_work_price'
                '.msg_timesheet_work_cost_price_work_date_uniq'),
            ]
        cls._order.insert(0, ('date', 'DESC'))

    def get_rec_name(self, name):
        return str(self.date)

    @staticmethod
    def default_cost_price():
        return Decimal(0)

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @classmethod
    def delete(cls, prices):
        Work = Pool().get('timesheet.work')
        super(WorkCostPrice, cls).delete(prices)
        Work._cost_prices_cache.clear()

    @classmethod
    def create(cls, vlist):
        Work = Pool().get('timesheet.work')
        prices = super(WorkCostPrice, cls).create(vlist)
        Work._cost_prices_cache.clear()
        return prices

    @classmethod
    def write(cls, *args):
        Work = Pool().get('timesheet.work')
        super(WorkCostPrice, cls).write(*args)
        Work._cost_prices_cache.clear()
